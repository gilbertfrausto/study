# Template Syntax(Views)
<!-- TOC -->

- [Template SyntaxViews](#template-syntaxviews)
  - [For loops](#for-loops)
      - [> #### AngularJS](#--angularjs)
      - [>#### Angular](#-angular)
      - [> #### React](#--react)
      - [>#### Vue](#-vue)
  - [If statements](#if-statements)
        - [> ##### AngularJS](#--angularjs)
        - [> ##### Angular](#--angular)
        - [> ##### React](#--react)
        - [> ##### Angular](#--angular)
  - [Events](#events)
        - [>##### AngularJS](#-angularjs)
        - [> ##### Angular](#--angular)
        - [> ##### React](#--react)
        - [> ##### Vue](#--vue)
- [Models Views](#models-views)
        - [> ##### AngularJs](#--angularjs)
        - [> ##### Angular](#--angular)
        - [> ##### React](#--react)
        - [> ##### Vue](#--vue)
- [Component Registration Controller](#component-registration-controller)
        - [> ##### AngularJs](#--angularjs)
        - [> ##### Angular](#--angular)
        - [> ##### React](#--react)
        - [> ##### Vue](#--vue)
- [Routing](#routing)
        - [> ##### AngularJS](#--angularjs)
        - [> ##### AngularJS](#--angularjs)
        - [> ##### React](#--react)
        - [> ##### Vue](#--vue)
- [HTTP request](#http-request)
        - [> ##### AngularJS](#--angularjs)
        - [> ##### Angular](#--angular)
        - [> ##### React](#--react)
        - [> ##### Vue](#--vue)
- [State Management/Services](#state-managementservices)

<!-- /TOC -->

## For loops
---------------------------------------
Code sample

```
  const persons = [{ name: 'yxz' } ]
```
---

> #### AngularJS
`ng-repeat="..."`

```
<div ng-repeat"person in $ctrl.persons">
  {{person}}
</div>
```

>#### Angular

`*`ngFor=" ... "

```
  <div *ngFor="let person of persons">
    {{ person }}
  </div>
```

> #### React
`.map()`

```
<div>
  {persons.map(person => (
    <p>{person.name}</p>
  ))}
</div>
```
>#### Vue

`v-for="..."`

```
  <span v-for="person of persons" :key="person.name">
    {{ person.name }}
  </span>
```


## If statements
---------------------------------------
Code sample

```
  const state = false;
```

> ##### AngularJS
`ng-if="..."`

```
<div ng-if="$ctrl.state">
  ...
</div>
```

> ##### Angular
`*ng-if="..."`

```
<div *ngIf="state">
  ...
</div>
```

> ##### React
`{state ...} | if (state)`

```
{ state && <component></component>}

if (state) {
  <component></component>
}
```

> ##### Angular
`*ng-if="..."`

```
<div v-if="state">
  ...
</div>
```


## Events
---------------------------------------
Code sample

`const clicker = (event) => {};`

----

>##### AngularJS
`ng-click=""`

```
<div ng-click="$ctrl.clicker($event)">
  ...
</div>
```

> ##### Angular
`(click)=""`

```
<div (click)="clicker()">
  ...
</div>
```

> ##### React
`onClick=""`

```
<div onClick={click}
  ...
</div>
```

> ##### Vue
`v-on:click="" | @click=""`

```
<div v-on:click="clicker"
  ...
</div>

<div @click="clicker"
  ...
</div>
```


# Models (Views)
---------------------------------------
```
  const customer = {
    firstName: 'abc'  
  }
```

----


> ##### AngularJs
`ng-model= ''`

```
  <input ng-model="customer.firstName" />
```

> ##### Angular
`[(ngModel)] = ''`

```
  <input [(ngModel)]="customer.firstName" />
```


> ##### React
`value={} `

```
  <input type="text" value={this.state.customer.firstName} onChange={this.handleChange} />
```

> ##### Vue
`v-model='' `

```
  <input v-model="customer.firstName" />
```

# Component Registration (Controller)
---------------------------------------

> ##### AngularJs
`angular.component();`

```
  class NavCtrl {
    static get $inject(){ return ['$scope']; }
    constructor() {}
    $onInit() {}
  }

  angular.module('app.nav', [])
    .component('navBar', {
        templateUrl: 'app/components/nav/nav.component.html',
        controller: NavCtrl,
        bindings: {}
    });

```

> ##### Angular
`@Component()`

```
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  constructor() {}
  ngOnInit() {}
}
```


> ##### React
`function()`

```
export default function Nav() {
  return (
    <div>...</div>
  )
}
```

> ##### Vue
`component.vue`

```
<template>
  <div>Nav</div>
</template>

<script>
  export default {
    name: 'Nav',
    props: [],
    data: () {
      return {};
    }
  }
</script>

<style llang="scss" scoped></style>
```


# Routing

> ##### AngularJS

`$stateProvider`

```
angular.module('app')
    .config(($stateProvider, $urlRouterProvider) => {
      $stateProvider
          .state('main', {
              url: "/",
              template: '<main></main>',
              data: 'Main'
              // resolve: { authenticate: authenticate }
          })
```

> ##### AngularJS

` `

```
const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

```

> ##### React
`BrowserRouter`

```
  import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';

  export default function App() {
  return (
    <Router>
      <div className="app">
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
        </Switch>
      </div>
    </Router>
  )
}

```

> ##### Vue

`new Router()`

```
export default new Router({
  routes: [
    {
        path: '/',
        name: 'home',
        component: Home
    },
  ]
});
```

# HTTP request

> ##### AngularJS
`$http`

```
  $http({
    method: 'GET',
    url: '/someUrl'
  }).then(function successCallback(response) {
      // this callback will be called asynchronously
      // when the response is available
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });

```

> ##### Angular
```
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Hero } from './hero';
import { MessageService } from './message.service';


@Injectable({ providedIn: 'root' })
export class HeroService {

  private heroesUrl = 'api/heroes';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET heroes from the server */
  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.heroesUrl)
      .pipe(
        tap(_ => this.log('fetched heroes')),
        catchError(this.handleError<Hero[]>('getHeroes', []))
      );
  }
}
```

> ##### React
* Native or Axios

> ##### Vue
* Native or Axios

# State Management/Services

TODO